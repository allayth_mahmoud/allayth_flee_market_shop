import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Controller/providers/cart.dart' show Cart;
import '../widgets/cart_item.dart';
import '../../Controller/providers/order.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);

    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        actions: [IconButton(
          icon: Icon(Icons.shopping_cart, color: Colors.transparent,),
          onPressed: () {
          },
        ),],
      elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(child: Text('Your Cart' , style: TextStyle(fontWeight: FontWeight.bold))),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: media.size.height * 0.15,
            child: Card(
              margin: EdgeInsets.all(15),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Total',
                      style: TextStyle(fontSize: 20),
                    ),
                    Spacer(),
                    Chip(
                      label: Text(
                        '\$${cart.totalAmount.toStringAsFixed(2)}',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      backgroundColor: Theme.of(context).hintColor,
                    ),
                    OrderButton(cart: cart)
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height:  media.size.height * 0.002,),
          Expanded(
            child: SizedBox(
              height: media.size.height * 0.83,
              child: ListView.builder(
                itemBuilder: (context, index) => SizedBox(
                  height: media.size.height * 0.14,
                  child: CartItem(
                      title: cart.items.values.toList()[index].title,
                      price: cart.items.values.toList()[index].price,
                      id: cart.items.values.toList()[index].id,
                      productId: cart.items.keys.toList()[index],
                      quantity: cart.items.values.toList()[index].quantity),
                ),
                itemCount: cart.items.length,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class OrderButton extends StatefulWidget {
  const OrderButton({
    required this.cart,
  });

  final Cart cart;

  @override
  _OrderButtonState createState() => _OrderButtonState();
}

class _OrderButtonState extends State<OrderButton> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: _isLoading ? CircularProgressIndicator() : Text('ORDER NOW', style: TextStyle(color: Theme.of(context).hintColor),),
      onPressed: (widget.cart.totalAmount <= 0 || _isLoading)
          ? null
          : () async {
        setState(() {
          _isLoading = true;
        });
        await Provider.of<Orders>(context, listen: false).addOrder(
          widget.cart.items.values.toList(),
          widget.cart.totalAmount,
        );
        setState(() {
          _isLoading = false;
        });
        widget.cart.clear();
      },
      //style: ButtonStyle(textStyle: TextStyle),
     // textColor: Theme.of(context).primaryColor,
    );
  }
}








/*
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/cart.dart' as ca;

import '../providers/order.dart';
import '../widgets/cart_item.dart';

class CartScreen extends StatelessWidget {
  static String routeName = '/cart_screen';

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<ca.Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Chip(
                    label: Text(
                      '\$${cart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  TextButton(
                    child: Text(
                      'ORDER NOW',
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    onPressed: () {
                      Provider.of<Orders>(context, listen: false).addOrder(
                        cart.items.values.toList(),
                        cart.totalAmount,
                      );
                      cart.clear();
                    },

                    //textColor: Theme.of(context).primaryColor,
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) => CartItem(
                  title: cart.items.values.toList()[index].title,
                  price: cart.items.values.toList()[index].price,
                  id: cart.items.values.toList()[index].id,
                  productId: cart.items.keys.toList()[index],
                  quantity: cart.items.values.toList()[index].quantity),
              itemCount: cart.items.length,
            ),
          ),
        ],
      ),
    );
  }
}
*/