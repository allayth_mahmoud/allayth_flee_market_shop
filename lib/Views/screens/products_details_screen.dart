import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Controller/providers/products.dart';

class ProductsDetailsScreen extends StatelessWidget {
  static const routeName = '/products_details_screen';

  @override
  Widget build(BuildContext context) {
    final productId =
        ModalRoute.of(context)?.settings.arguments as String; // this is id
    final loadedProduct =
        Provider.of<Products>(context, listen: false).findById(productId);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.transparent,
        scrolledUnderElevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: Image.network(
                    loadedProduct.imageUrl,
                    fit: BoxFit.contain,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: IconButton(
                    icon: Icon(
                      Icons.close,
                      color: Colors.black,
                      size: 30,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            ),
          ),
          //  Divider(endIndent: 33, indent: 33, thickness: 2),
          SizedBox(height: 10),
          Text(
            loadedProduct.title,
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10),
          Text(
            '\$${loadedProduct.price}',
            style: TextStyle(
              color: Colors.grey,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 10),
          Divider(endIndent: 33, indent: 33, thickness: 2),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
              ),
              child: Padding(
                padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Description',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 10),
                      Text(
                        loadedProduct.description,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}



// class ProductsDetailsScreen extends StatelessWidget {
//   static const routeName = '/products_details_screen';

//   @override
//   Widget build(BuildContext context) {
//     final productId =  ModalRoute.of(context)?.settings.arguments as String; // this is id
//     final loadedProduct = Provider.of<Products>(context, listen: false)
//         .findById(productId);
//     return Scaffold(
//       appBar: AppBar(
//         actions: [IconButton(
//           icon: Icon(Icons.shopping_cart, color: Colors.transparent,),
//           onPressed: () {
//           },
//         ),],
//         elevation: 0,
//         backgroundColor: Colors.transparent,
//         title: Center(child: Text(loadedProduct.title, style: TextStyle(fontWeight: FontWeight.bold),)),
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           children: <Widget>[
//             Container(
//               height: 300,
//               width: double.infinity,
//               child: Image.network(
//                 loadedProduct.imageUrl,
//                 fit: BoxFit.cover,
//               ),
//             ),
//             SizedBox(height: 10),
//             Text(
//               '\$${loadedProduct.price}',
//               style: TextStyle(
//                 color: Colors.grey,
//                 fontSize: 20,
//               ),
//             ),
//             SizedBox(
//               height: 10,
//             ),
//             Container(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               width: double.infinity,
//               child: Text(
//                 loadedProduct.description,
//                 textAlign: TextAlign.center,
//                 softWrap: true,
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
