import 'package:allayth_shop/Views/widgets/product_grid.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Controller/providers/auth.dart';
import '../../Controller/providers/cart.dart';
import '../../Controller/providers/product.dart';
import '../screens/products_details_screen.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context).size;
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    final authData = Provider.of<Auth>(context, listen: false);
    print('Product rebuild');

    return Container(
      height: media.height * 0.9,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.3),
            spreadRadius: 1,
            blurRadius: 5,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: media.height * 0.26,
            child: Expanded(
              flex: 3,
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(
                    ProductsDetailsScreen.routeName,
                    arguments: product.id,
                  );
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    product.imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
          Container(
            //color: Colors.grey,
            height: media.height * 0.05,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Flexible(
                    child: Text(
                      product.title,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  Consumer<Product>(
                    builder: (ctx, i, _) => IconButton(
                      icon: product.isFavorite
                          ? Icon(Icons.favorite, color: Colors.red)
                          : Icon(Icons.favorite_border),
                      onPressed: () {
                        product.toggleFavoritesStatus(
                          authData.token as String,
                          authData.userId as String,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          Divider(endIndent: 33, indent: 33),
          Container(
            //   color: Colors.grey,
            height: media.height * 0.05,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    '\$${product.price}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.shopping_cart,
                      color: Colors.black,
                    ),
                    onPressed: () {
                      cart.addItem(
                        product.id,
                        product.title,
                        product.price,
                      );
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text('Item added to cart!'),
                          duration: Duration(seconds: 2),
                          action: SnackBarAction(
                            label: 'UNDO',
                            onPressed: () {
                              cart.removeSingleItem(product.id);
                            },
                          ),
                        ),
                      );
                    },
                    color: Theme.of(context).primaryColor,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
// class ProductItem extends StatelessWidget {
//   // final String id;
//   // final String title;
//   // final String imageUrl;
//   //
//   // ProductItem(this.id, this.title, this.imageUrl);

//   @override
//   Widget build(BuildContext context) {
//     final product = Provider.of<Product>(context, listen: false);
//     final cart = Provider.of<Cart>(context, listen: false);
//     final authData = Provider.of<Auth>(context, listen: false);
//     print('procuct rebuild');
//     return ClipRRect(
//       borderRadius: BorderRadius.circular(10),
//       child: GridTile(
//         child: GestureDetector(
//           onTap: () {
//             Navigator.of(context).pushNamed(
//               ProductsDetailsScreen.routeName,
//               arguments: product.id,
//             );
//           },
//           child: Image.network(
//             product.imageUrl,
//             fit: BoxFit.cover,
//           ),
//         ),
//         footer: GridTileBar(
//           backgroundColor: Colors.black87,
//           leading: Consumer<Product>(
//             builder: (ctx, i, _) => IconButton(
//               icon: product.isFavorite
//                   ? Icon(Icons.favorite)
//                   : Icon(Icons.favorite_border),
//               color: product.isFavorite
//                   ?Colors.red : Theme.of(context).hintColor,
//               onPressed: () {
//                 product.toggleFavoritesStatus(authData.token as String, authData.userId as String);
//               },
//             ),
//           ),
//           title: Text(
//             product.title,
//             textAlign: TextAlign.center,
//           ),
//           trailing: IconButton(
//             icon: Icon(
//               Icons.shopping_cart,
//             ),
//             onPressed: () {
//               cart.addItem(
//                 product.id,
//                 product.title,
//                 product.price,
//               );
//               ScaffoldMessenger.of(context).hideCurrentSnackBar();
//               ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//                 content: Text('new item just added! '),
//                 duration: Duration(seconds: 2),
//                 action: SnackBarAction(
//                   label: 'UNDO',
//                   onPressed: () {
//                     cart.removeSingleItem(product.id);
//                   },
//                 ),
//               ));
//             },
//             color: Theme.of(context).hintColor,
//           ),
//         ),
//       ),
//     );
//   }
// }
