import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerWidgets {
  static Shimmer productItemsShimmer(var h, var w) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: SingleChildScrollView(
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(33)),
              child: Container(
                //color: Colors.black45,
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Column(children: [
                  Container(
                    height: h * 0.25,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: h * 0.06,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                ]),
              ),
            ),
            SizedBox(
              height: h * 0.03,
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(33)),
              child: Container(
                //color: Colors.black45,
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Column(children: [
                  Container(
                    height: h * 0.25,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: h * 0.06,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                ]),
              ),
            ),
            SizedBox(
              height: h * 0.03,
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(33)),
              child: Container(
                //color: Colors.black45,
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Column(children: [
                  Container(
                    height: h * 0.25,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: h * 0.06,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                ]),
              ),
            ),
            SizedBox(
              height: h * 0.03,
            ),
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(33)),
              child: Container(
                //color: Colors.black45,
                padding: const EdgeInsets.symmetric(horizontal: 6.0),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Column(children: [
                  Container(
                    height: h * 0.25,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: h * 0.06,
                    width: w * 0.8,
                    color: Colors.grey,
                  ),
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Shimmer productUserItemsShimmer(var h, var w) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(33)),
        child: Container(
          //color: Colors.black45,
          padding: const EdgeInsets.symmetric(horizontal: 6.0),
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: Column(children: [
            Container(
              height: h * 0.25,
              width: w * 0.8,
              color: Colors.grey,
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              height: h * 0.06,
              width: w * 0.8,
              color: Colors.grey,
            ),
          ]),
        ),
      ),
    );
  }

  static Shimmer orderCardShimmer() {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: Container(
        padding: const EdgeInsets.all(5),
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: ListTile(
          contentPadding: const EdgeInsets.all(8.0),
          leading: const Icon(
            Icons.account_circle,
            size: 50,
            color: Colors.white,
          ),
          title: Container(
            padding: const EdgeInsets.fromLTRB(0, 5, 5, 5),
            height: 10.0,
            color: Colors.white,
          ),
          subtitle: Row(
            children: [
              Expanded(
                child: Container(
                  height: 10.0,
                  color: Colors.white,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
