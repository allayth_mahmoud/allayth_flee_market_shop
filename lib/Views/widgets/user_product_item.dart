import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../screens/edit_product_screen.dart';
import '../../Controller/providers/products.dart';

class UserProductItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;
  final double price;

  UserProductItem(
      {required this.id,
      required this.title,
      required this.imageUrl,
      required this.price});

  @override
  Widget build(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    var media = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.blueGrey[100],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
              child: Image.network(
                imageUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            height: media.height * 0.138,
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
                //SizedBox(height: 10),
                Text(
                  '\$${price.toStringAsFixed(2)}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
                //   SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                          EditProductScreen.routeName,
                          arguments: id,
                        );
                      },
                      child: Icon(Icons.edit) /*Text('Edit')*/,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (ctx) => AlertDialog(
                            title: Text('Are you sure?'),
                            content: Text(
                                'Do you want to remove the item from the shop?'),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(ctx).pop(false);
                                },
                                child: Text('No'),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.of(ctx).pop(true);
                                },
                                child: Text('Yes'),
                              ),
                            ],
                          ),
                        ).then((value) {
                          if (value) {
                            Provider.of<Products>(context, listen: false)
                                .deleteProduct(id);
                          }
                        });
                      },
                      child: Icon(Icons.delete),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
    // ListTile(
    //   title: Text(title),
    //   leading: CircleAvatar(
    //     backgroundImage: NetworkImage(imageUrl),
    //   ),
    //   trailing: Container(
    //     width: 100,
    //     child: Row(
    //       children: <Widget>[
    //         IconButton(
    //           icon: Icon(Icons.edit , color: Theme.of(context).hintColor,),

    //           onPressed: () {
    //             Navigator.of(context)
    //                 .pushNamed(EditProductScreen.routeName, arguments: id);
    //           },
    //           color: Theme.of(context).primaryColor,
    //         ),
    //         IconButton(
    //           icon: Icon(Icons.delete),
    //           onPressed: () async {
    //             try {
    //               await Provider.of<Products>(context, listen: false)
    //                   .deleteProduct(id);
    //             } catch (error) {
    //               scaffold.showSnackBar(
    //                 SnackBar(
    //                   content: Text('Deleting failed!', textAlign: TextAlign.center,),
    //                 ),
    //               );
    //             }
    //           },
    //           color: Theme.of(context).errorColor,
    //         ),
    //       ],
    //     ),
    //   ),
    // );
  }
}
