import 'package:allayth_shop/Controller/providers/auth.dart';
import 'package:allayth_shop/Views/screens/auth_screen.dart';
import 'package:allayth_shop/Views/screens/orders_screen.dart';
import 'package:allayth_shop/Views/screens/user_products_screen.dart';

import 'Views/screens/cart_screen.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Controller/providers/cart.dart';
import 'Controller/providers/order.dart';
import 'Controller/providers/products.dart';
import 'Views/screens/edit_product_screen.dart';
import 'Views/screens/products_details_screen.dart';
import 'Views/screens/products_overview_screen.dart';
import 'Views/screens/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => Auth(),
        ),
        ChangeNotifierProxyProvider<Auth, Products>(
          create: (ctx) => Products('', '', []),
          update: (context, auth, previousProducts) => Products(
              auth.token,
              auth.userId,
              previousProducts == null ? [] : previousProducts.items),
        ),
        ChangeNotifierProvider(
          create: (context) => Cart(),
        ),
        ChangeNotifierProxyProvider<Auth, Orders>(
          create: (context) => Orders('', [], ''),
          update: (ctx, auth, previousProducts) => Orders(
              auth.token,
              previousProducts == null ? [] : previousProducts.orders,
              auth.userId),
        ),
      ],
      child: Consumer<Auth>(
        builder: (ctx, auth, _) => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Allayth Store',
          theme: ThemeData(
            primarySwatch: Colors.grey,
            hintColor: Colors.teal,
            fontFamily: 'Lato',
          ),
          home: auth.isAuth
              ? ProductsOverviewScreen()
              : FutureBuilder(
                  future: auth.tryAutoLogin(),
                  builder: (ctx, authResultSnapshot) =>
                      authResultSnapshot.connectionState ==
                              ConnectionState.waiting
                          ? SplashScreen()
                          : AuthScreen(),
                ),
          routes: {
            ProductsDetailsScreen.routeName: (ctx) => ProductsDetailsScreen(),
            CartScreen.routeName: (ctx) => CartScreen(),
            OrdersScreen.routeName: (ctx) => OrdersScreen(),
            UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
            EditProductScreen.routeName: (ctx) => EditProductScreen(),
          },
        ),
      ),
    );
  }
}
