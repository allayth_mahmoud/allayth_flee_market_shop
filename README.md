# allayth_flea_shop

Welcome to our shop flea market project built with Flutter! Our app provides a platform for users to buy and sell products in an easy, efficient, and secure way. We've implemented the Provider package for state management and Firebase for authentication and data storage, ensuring a fast, reliable, and secure user experience.

Our app allows users to sign up and log in using email and password authentication, providing a secure and personalized experience. Once logged in, users can browse a wide range of products and add them to their cart. We also provide a simple and intuitive checkout process, allowing users to complete their purchases quickly and easily.

For sellers, our app provides a simple and intuitive platform for listing and selling their products. Sellers can easily create listings, set prices, and manage their inventory.  

Our app uses Firebase as the backend for storing product data, user information, and transaction history.

Whether you're a buyer looking for unique and interesting products, or a seller looking to reach a wider audience, our shop flea market app has got you covered. We're committed to providing the best possible experience for our users, and we hope you enjoy using our app as much as we enjoyed building it!
